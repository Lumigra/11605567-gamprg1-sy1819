// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class C_Plus_Plus_DemoTarget : TargetRules
{
	public C_Plus_Plus_DemoTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("C_Plus_Plus_Demo");
	}
}
