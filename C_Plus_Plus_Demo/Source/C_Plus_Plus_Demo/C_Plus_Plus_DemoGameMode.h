// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "C_Plus_Plus_DemoGameMode.generated.h"

UCLASS(minimalapi)
class AC_Plus_Plus_DemoGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AC_Plus_Plus_DemoGameMode();
};



