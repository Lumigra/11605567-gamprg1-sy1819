// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "C_Plus_Plus_DemoGameMode.h"
#include "C_Plus_Plus_DemoCharacter.h"
#include "UObject/ConstructorHelpers.h"

AC_Plus_Plus_DemoGameMode::AC_Plus_Plus_DemoGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
