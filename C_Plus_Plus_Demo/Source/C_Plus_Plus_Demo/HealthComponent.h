// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class C_PLUS_PLUS_DEMO_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		int32 TotalHealth;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
		int32 CurrentHealth;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION(BlueprintCallable, Category = "Health")
		void TakeDamage(int32 damage);
	UFUNCTION(BlueprintCallable, Category = "Health")
		int32 GetCurrentHealth();
	UFUNCTION(BlueprintImplementableEvent, Category = "Health")
		void KillPlayer();
};
