// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class C_Plus_Plus_DemoEditorTarget : TargetRules
{
	public C_Plus_Plus_DemoEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("C_Plus_Plus_Demo");
	}
}
