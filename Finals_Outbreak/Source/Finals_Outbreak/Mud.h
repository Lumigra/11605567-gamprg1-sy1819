// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Hazard.h"
#include "Mud.generated.h"

/**
 * 
 */
UCLASS()
class FINALS_OUTBREAK_API AMud : public AHazard
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hazard")
		float SlowSpeed;

};
