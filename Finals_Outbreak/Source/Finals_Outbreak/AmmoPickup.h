// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "Weapon.h"
#include "AmmoPickup.generated.h"

/**
 * 
 */
UCLASS()
class FINALS_OUTBREAK_API AAmmoPickup : public APickup
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
		int32 AmmoValue;
	
public:
	UFUNCTION(BlueprintCallable, Category = "Pickup")
		void GiveAmmo(AWeapon * weapon);
	
};
