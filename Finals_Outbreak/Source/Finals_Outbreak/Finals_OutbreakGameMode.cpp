// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Finals_OutbreakGameMode.h"
#include "Finals_OutbreakHUD.h"
#include "Finals_OutbreakCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFinals_OutbreakGameMode::AFinals_OutbreakGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFinals_OutbreakHUD::StaticClass();
}
