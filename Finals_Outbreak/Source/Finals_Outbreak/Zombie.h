// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Unit.h"
#include "Zombie.generated.h"

/**
 * 
 */
UCLASS()
class FINALS_OUTBREAK_API AZombie : public AUnit
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
		APawn* target;
	
	
};
