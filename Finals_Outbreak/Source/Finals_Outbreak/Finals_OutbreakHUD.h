// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Finals_OutbreakHUD.generated.h"

UCLASS()
class AFinals_OutbreakHUD : public AHUD
{
	GENERATED_BODY()

public:
	AFinals_OutbreakHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

