// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "Health.h"
#include "HealthPickup.generated.h"

/**
 * 
 */
UCLASS()
class FINALS_OUTBREAK_API AHealthPickup : public APickup
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
		int32 HealValue;

public:
	UFUNCTION(BlueprintCallable, Category = "Pickup")
		void GiveHealth(UHealth * health);
	
	
};
