// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Finals_OutbreakGameMode.generated.h"

UCLASS(minimalapi)
class AFinals_OutbreakGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFinals_OutbreakGameMode();
};



