// Fill out your copyright notice in the Description page of Project Settings.

#include "HealthPickup.h"


void AHealthPickup::GiveHealth(UHealth * health)
{
	health->Heal(HealValue);
}