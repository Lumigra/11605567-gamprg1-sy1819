// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Hazard.h"
#include "Health.h"
#include "Fire.generated.h"

/**
 * 
 */
UCLASS()
class FINALS_OUTBREAK_API AFire : public AHazard
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hazard")
		float damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hazard")
		float DamageRate;
	
	
public:
	UFUNCTION(BlueprintCallable, Category = "Hazard")
		void DamageCharacter(UHealth* health);
};
