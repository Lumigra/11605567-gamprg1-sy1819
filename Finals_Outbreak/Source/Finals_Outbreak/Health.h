// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Health.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FINALS_OUTBREAK_API UHealth : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealth();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		int32 MaxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
		int32 CurrentHealth;


private:

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION(BlueprintCallable, Category = "Health")
		void TakeDamage(float damage);
	UFUNCTION(BlueprintCallable, Category = "Health")
		void Heal(float healValue);
	UFUNCTION(BlueprintPure, Category = "Health")
		int32 GetHealth();
	
};
